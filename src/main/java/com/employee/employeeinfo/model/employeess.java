package com.employee.employeeinfo.model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.persistence.Table;
import javax.validation.constraints.Size;



@Entity
@Table(name="emptable")
public class employeess extends AuditModel
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Size(max = 100)
	@Column(updatable = true)
	@NotNull
	private String empName;
	
	@Size(max = 100)
	@Column(updatable = true)
	@NotNull
	private String empRole;
	
	@Size(max = 100)
	@Column(updatable = true)
	@NotNull
	private String empExp;
	
	
	@Column(updatable = true)
	private Long empSalary;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpRole() {
		return empRole;
	}

	public void setEmpRole(String empRole) {
		this.empRole = empRole;
	}

	public String getEmpExp() {
		return empExp;
	}

	public void setEmpExp(String empExp) {
		this.empExp = empExp;
	}

	public Long getEmpSalary() {
		return empSalary;
	}

	public void setEmpSalary(Long empSalary) {
		this.empSalary = empSalary;
	}

}
