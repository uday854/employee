package com.employee.employeeinfo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.employee.employeeinfo.Repository.EmployeeRepository;
import com.employee.employeeinfo.model.employeess;

@RestController
public class Employeecontroller 
{
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@GetMapping("/api/get/getallemployees")
	public List<employeess> getAllEmployees()
	{
		return employeeRepository.findAll();
	}
	
	@GetMapping("/api/get/getallemployees/{id}")
	public employeess getEmployeess(@PathVariable(value = "id") Long id )
	{
		return employeeRepository.getOne(id);
	}
	
	@PostMapping("/api/post/saveemployeess")
	public employeess saveEmployeess(@RequestBody employeess employee)
	{
		return employeeRepository.save(employee); 
	}
	
	@PutMapping("/api/put/updateemployeess")
	public employeess updateEmployeess(@RequestBody employeess employee)
			{
		       return employeeRepository.save(employee);
			}
	
	@DeleteMapping("/api/delete/deleteemployeess/{userId}")
	public void deleteEmployeess(@PathVariable(value = "userId") Long id)
	{
		employeess emp= employeeRepository.getOne(id);
		 employeeRepository.delete(emp);
	}
	
	

}
