package com.employee.employeeinfo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.employeeinfo.model.employeess;

@Repository
public interface EmployeeRepository extends JpaRepository<employeess, Long> 
{

}
